package com.alexandro45.resistorchecker;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.hardware.Camera;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    SurfaceView sv;
    SurfaceHolder holder;
    HolderCallback holderCallback;
    Camera camera;
    final int GOLDEN = Color.parseColor("#A19D60");
    final int BROWN = Color.parseColor("#421F1D");
    final int ORANGE = Color.parseColor("#C86B04");
    final int YELOW = Color.parseColor("#C8CC43");
    final int GREEN = Color.parseColor("#29694E");
    final int BLUE = Color.parseColor("#3E5EA9");
    final int VIOLET = Color.parseColor("#7133C7");
    final int GRAY = Color.parseColor("#7581B5");
    final int WHITE = Color.parseColor("#ffffff");
    final int SILVER = Color.parseColor("#97A6CF");
    final int BLACK = Color.parseColor("#000000");
    final int BOUNS = 10;

    -6185632
    -12443875
    -3642620
            12-03 20:58:15.364 6796-6796/? I/resistorChecker: -3617725
            12-03 20:58:15.364 6796-6796/? I/resistorChecker: -14063282
            12-03 20:58:15.364 6796-6796/? I/resistorChecker: -12689751
            12-03 20:58:15.364 6796-6796/? I/resistorChecker: -9358393
            12-03 20:58:15.364 6796-6796/? I/resistorChecker: -9076299
            12-03 20:58:15.364 6796-6796/? I/resistorChecker: -1
            12-03 20:58:15.364 6796-6796/? I/resistorChecker: -6838577
            12-03 20:58:15.364 6796-6796/? I/resistorChecker: -16777216
            12-03 20:58:15.364 6796-6796/? I/resistorChecker: -6185632

    final int CAMERA_ID = 0;
    final boolean FULL_SCREEN = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.main);

        sv = (SurfaceView) findViewById(R.id.surfaceView);
        holder = sv.getHolder();
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        holderCallback = new HolderCallback();
        holder.addCallback(holderCallback);
        Log.i("resistorChecker",GOLDEN + "");
        Log.i("resistorChecker",BROWN + "");
        Log.i("resistorChecker",ORANGE + "");
        Log.i("resistorChecker",YELOW + "");
        Log.i("resistorChecker",GREEN + "");
        Log.i("resistorChecker",BLUE + "");
        Log.i("resistorChecker",VIOLET + "");
        Log.i("resistorChecker",GRAY + "");
        Log.i("resistorChecker",WHITE + "");
        Log.i("resistorChecker",SILVER + "");
        Log.i("resistorChecker",BLACK + "");
        Log.i("resistorChecker",GOLDEN + "");
    }

    @Override
    protected void onResume() {
        super.onResume();
        camera = Camera.open(CAMERA_ID);
        Camera.Parameters parameters = camera.getParameters();
        parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
        camera.setParameters(parameters);
        setPreviewSize(FULL_SCREEN);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (camera != null)
            camera.release();
        camera = null;
    }

    public void onCl(View view) {

        final Camera.ShutterCallback shutterCallback = new Camera.ShutterCallback() {

            @Override
            public void onShutter() {
                // TODO Auto-generated method stub
            }
        };

        final Camera.PictureCallback rawPictureCallback = new Camera.PictureCallback() {

            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                // TODO Auto-generated method stub
            }
        };

        final Camera.PictureCallback mPicture = new Camera.PictureCallback() {

            public void onPictureTaken(final byte[] data, final Camera camera) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            int rotation = getWindowManager().getDefaultDisplay().getRotation();
                            int degrees = 0;
                            switch (rotation) {
                                case Surface.ROTATION_0:
                                    degrees = 0;
                                    break;
                                case Surface.ROTATION_90:
                                    degrees = 90;
                                    break;
                                case Surface.ROTATION_180:
                                    degrees = 180;
                                    break;
                                case Surface.ROTATION_270:
                                    degrees = 270;
                                    break;
                            }

                            int result = 0;

                            // получаем инфо по камере cameraId
                            Camera.CameraInfo info = new Camera.CameraInfo();
                            Camera.getCameraInfo(CAMERA_ID, info);

                            // задняя камера
                            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                                result = ((360 - degrees) + info.orientation);
                            } else
                                // передняя камера
                                if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                                    result = ((360 - degrees) - info.orientation);
                                    result += 360;
                                }
                            result = result % 360;
                            Matrix matrix = new Matrix();
                            matrix.postRotate(result);
                            Bitmap bitmap = BitmapFactory.decodeByteArray(data,0,data.length);
                            bitmap = Bitmap.createBitmap(bitmap,0,0,bitmap.getWidth(),bitmap.getHeight(),matrix,false);
                            int width = bitmap.getWidth();
                            int height = bitmap.getHeight();
                            int size = width * height;
                            int[] pixels = new int[size];
                            bitmap.getPixels(pixels, 0, width, 0, 0, width, height);
                            for (int i = 0; i < size; i++) {
                                int color = pixels[i];

                                Log.i("resistorChecker",color + "");

                                //получаем значение красного канала
                                int r = Color.red(color);

                                //получаем значение зеленого канала
                                int g = Color.green(color);

                                //получаем значение синего канала
                                int b = Color.blue(color);

                                int c = Color.rgb(r,g,b);

                                /*for (int j = c-BOUNS; j <= c+BOUNS; j++){
                                    switch (j){
                                        case Color.RED:
                                            pixels[i] = Color.RED;
                                            break;
                                        case GOLDEN:
                                            break;
                                    }
                                }*/

                                if(r > g && r > b){
                                    pixels[i] = Color.RED;
                                }

                                if(g > r && g > b){
                                    pixels[i] = Color.GREEN;
                                }

                                if(b > g && b > r){
                                    pixels[i] = Color.BLUE;
                                }

                                //вычисляем полутоновую яркость по формуле перехода RGB to YUV
                                //double luminance = (0.299 * r + 0.0f + 0.587 * g + 0.0f + 0.114 * b + 0.0f);
                                //pixels[i] = (int) luminance;
                            }
                            //bitmap = Bitmap.createBitmap(pixels,bitmap.getWidth(),bitmap.getHeight(),Bitmap.Config.RGB_565);
                            byte[] bytes;
                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.PNG,100,byteArrayOutputStream);
                            bytes = byteArrayOutputStream.toByteArray();
                            String s = Environment.getExternalStorageDirectory() + "/pictures/";
                            new File(s).mkdir();
                            FileOutputStream fos = new FileOutputStream(s + System.currentTimeMillis() + ".jpg");
                            fos.write(bytes);
                            fos.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
                //camera.startPreview();
            }
        };

        camera.autoFocus(new Camera.AutoFocusCallback() {
            @Override
            public void onAutoFocus(boolean b, Camera camera) {
                camera.takePicture(shutterCallback,rawPictureCallback,mPicture);
            }
        });


    }

    class HolderCallback implements SurfaceHolder.Callback {

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            try {
                camera.setPreviewDisplay(holder);
                camera.startPreview();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width,
                                   int height) {
            camera.stopPreview();
            setCameraDisplayOrientation(CAMERA_ID);
            try {
                camera.setPreviewDisplay(holder);
                camera.startPreview();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {

        }

    }

    void setPreviewSize(boolean fullScreen) {

        // получаем размеры экрана
        Display display = getWindowManager().getDefaultDisplay();
        boolean widthIsMax = display.getWidth() > display.getHeight();

        // определяем размеры превью камеры
        Camera.Size size = camera.getParameters().getPreviewSize();

        RectF rectDisplay = new RectF();
        RectF rectPreview = new RectF();

        // RectF экрана, соотвествует размерам экрана
        rectDisplay.set(0, 0, display.getWidth(), display.getHeight());

        // RectF первью
        if (widthIsMax) {
            // превью в горизонтальной ориентации
            rectPreview.set(0, 0, size.width, size.height);
        } else {
            // превью в вертикальной ориентации
            rectPreview.set(0, 0, size.height, size.width);
        }

        Matrix matrix = new Matrix();
        // подготовка матрицы преобразования
        if (!fullScreen) {
            // если превью будет "втиснут" в экран (второй вариант из урока)
            matrix.setRectToRect(rectPreview, rectDisplay,
                    Matrix.ScaleToFit.START);
        } else {
            // если экран будет "втиснут" в превью (третий вариант из урока)
            matrix.setRectToRect(rectDisplay, rectPreview,
                    Matrix.ScaleToFit.START);
            matrix.invert(matrix);
        }
        // преобразование
        matrix.mapRect(rectPreview);

        // установка размеров surface из получившегося преобразования
        sv.getLayoutParams().height = (int) (rectPreview.bottom);
        sv.getLayoutParams().width = (int) (rectPreview.right);
    }

    void setCameraDisplayOrientation(int cameraId) {
        // определяем насколько повернут экран от нормального положения
        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result = 0;

        // получаем инфо по камере cameraId
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);

        // задняя камера
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
            result = ((360 - degrees) + info.orientation);
        } else
            // передняя камера
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                result = ((360 - degrees) - info.orientation);
                result += 360;
            }
        result = result % 360;
        camera.setDisplayOrientation(result);
    }
}
